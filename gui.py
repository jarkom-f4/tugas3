import tkinter as tk
from tkinter import messagebox
from tkinter import ttk
import time
import scheduler
import threading
from functools import partial

title_font = ("times",30,"normal")
default_font = ("times",20,"normal")

# modified scrollable frame class from https://blog.tecladocode.com/tkinter-scrollable-frames/
class ScrollableFrame(tk.Frame):
    def __init__(self, container, *args, **kwargs):
        superKwargs = kwargs.copy()
        if "canvas_width" in kwargs:
            del superKwargs["canvas_width"]

        super().__init__(container, *args, **superKwargs)

        canvas = tk.Canvas(self)
        #self.canvas = canvas
        if "bg" in kwargs:
            canvas.config(bg = kwargs["bg"])

        if "canvas_width" in kwargs:
            canvas.config(width = kwargs["canvas_width"])
        
        scrollbar = ttk.Scrollbar(self, orient="vertical", command=canvas.yview)
        self.scrollable_frame = tk.Frame(canvas)

        self.scrollable_frame.bind(
            "<Configure>",
            lambda e: canvas.configure(
                scrollregion=canvas.bbox("all")
            )
        )

        canvas.create_window((0, 0), window=self.scrollable_frame, anchor="nw")

        canvas.configure(yscrollcommand=scrollbar.set)

        canvas.pack(side="left", fill="both", expand=True)
        scrollbar.pack(side="right", fill="y")
    

# workerLabel : tuple (worker, label) dari worker ini
# harus global karena dipake dalam thread. kalau local dan pass parameter lalu di update luar thread maka dia gak ke update
workerLabels = []

# monitor jika ada label ganti
def workersLabelsUpdate(container):
    
    global workerLabels
    while True:
        for worker, label in workerLabels:
            if worker.workerStatus != label.cget("text"):
                label.config(text = worker.getStatusStr())
        
        time.sleep(0.05)
    

# update list workers jika ada worker baru
def workersListUpdate(container):

    global workerLabels

    while True:
        actualWorkers = scheduler.getWorkersAsList()

        workerLabelsSet = set({})
        for worker, _ in workerLabels:
            workerLabelsSet.add(worker)

        for worker in actualWorkers:
            if worker not in workerLabelsSet:
                this_worker = tk.Label(container, text = worker.getStatusStr(), font = default_font, justify = tk.LEFT, anchor="w")
                #this_worker.grid(row=workerNum, column = 0, sticky = tk.W)
                this_worker.pack(fill='both')
                workerLabels.append((worker, this_worker))
        
        time.sleep(0.05)



# jobsLabel: tuple (job, label) dari worker ini
jobLabels = []

def jobsLabelsUpdate(container):
    
    global jobLabels
    while True:
        for job, label in jobLabels:
            if job.jobStatus != label.cget("text"):
                label.config(text = job.getStatusStr())
        
        time.sleep(0.05)

# ganti info di bawah GUI jadi job ini
def revealJobAnswer(event, answerField, job):
    answerField.config(text = job.getJobInfo())

# update list workers jika ada worker baru
def jobsListUpdate(container, answerField):

    global jobLabels

    while True:
        actualJobs = scheduler.getJobsAsList()

        jobLabelsSet = set({})
        for job, _ in jobLabels:
            jobLabelsSet.add(job)

        for job in actualJobs:
            if job not in jobLabelsSet:
                this_job = tk.Label(container, text = job.getStatusStr(), font = default_font, justify = tk.LEFT, anchor="w")
                this_job.pack(fill='both')
                jobLabels.append((job, this_job))

                # binding this to answer field
                this_job.bind("<Button-1>", partial(revealJobAnswer, job = job, answerField = answerField))
        
        time.sleep(0.05)

# monitor jika ada ganti
# def jobLabelsUpdate(container, jobsLabel):
    
#     for jobs, lab

def new_job(N_entry, coins_entry, target_entry):
    
    N_raw = N_entry.get()
    coins_raw = coins_entry.get()
    target_value_raw = target_entry.get()

    N = ""
    coins = ""
    target_value = ""

    try:
        N = int(N_raw)

        if N <= 0:
            tk.messagebox.showerror(title="Input Error", message="Number of coins must be a positive integer!")
            return
        
        if N > 1000:
            tk.messagebox.showerror(title="Input Error", message="Number of coins is too large! maximum number of coins is 1000")

    except:
        tk.messagebox.showerror(title="Input Error", message="Please enter a valid number of coins! Number of coins must be a positive integer")
        return
    
    try:
        coins = [int(x) for x in coins_raw.split(' ')]

        for coin in coins:
            if coin < 0:
                tk.messagebox.showerror(title="Input Error", message="Coins value must be positive integers!")
                return
        
        if len(coins) != N:
            tk.messagebox.showerror(title="Input Error", message="Number of coins you give does not match the number of coins you specified!")
            return

    except:
        tk.messagebox.showerror(title="Input Error", message="Please enter valid coin values! Make sure that each value is separated by exactly one space.")
        return

    try:
        target_value = int(target_value_raw)

        if target_value <= 0:
            tk.messagebox.showerror(title="Input Error", message = "target value must be a positive integer!")

        if target_value > 100000:
            tk.messagebox.showerror(title="Input Error", message = "target value is too large! maximum target value is 100000")
    except:
        tk.messagebox.showerror(title="Input Error", message = "Please enter a valid target value! Target value must be a positive integer")
    
    scheduler.new_job(N, coins, target_value)


if __name__ == "__main__":



    scheduler.start_server()

    root = tk.Tk()
    root.resizable(0,0)

    # creating title
    root.title("Dc3-S")
    title = tk.Frame(master = root)
    label = tk.Label(master = title, text = "Distributed Coin Change Computing System", font = title_font)
    label.pack()
    title.grid(row=0, column = 0, columnspan = 3)

    # creating jobs input
    jobsFrame = tk.Frame(master = root, borderwidth=2)
    newjob = tk.Label(master = jobsFrame, text = "New Job:", font = default_font )
    newjob.grid(row=0, column = 0, columnspan = 2, pady=(10, 10))
    
    number_of_coins = tk.Label(master = jobsFrame, text = "Number of Coins:", font = default_font, justify=tk.LEFT, anchor="w")
    number_of_coins.grid(sticky = tk.W, row = 1, column = 0)
    number_of_coins_input = tk.Entry(master = jobsFrame, font = default_font)
    number_of_coins_input.grid(row=1, column = 1, pady = (10, 10))

    coins = tk.Label(master = jobsFrame, text = "Coins Values (space separated):", font = default_font, justify=tk.LEFT, anchor="w")
    coins.grid(sticky = tk.W, row = 2, column = 0)
    coins_input = tk.Entry(master = jobsFrame, font = default_font)
    coins_input.grid(row=2, column = 1, pady = (10, 10))

    target_value = tk.Label(master = jobsFrame, text = "Target Value:", font = default_font, justify=tk.LEFT, anchor="w")
    target_value.grid(sticky = tk.W, row = 3, column = 0)
    target_value_input = tk.Entry(master = jobsFrame, font = default_font)
    target_value_input.grid(row=3, column = 1, pady = (10, 10))

    send_job = tk.Button(master = jobsFrame, text = "Send Job!", font = default_font, command = partial(new_job, number_of_coins_input, coins_input, target_value_input))
    send_job.grid(row=4, column = 1, sticky = tk.SE, pady = (10, 10))

    jobsFrame.grid(row=1, column=0, padx=(10, 10))

    # creating list of jobs
    listOfJobsFrame = tk.Frame(master = root)
    listOfJobsTitle = tk.Label(master = listOfJobsFrame, text = "Jobs (Click to see)", font = default_font)
    listOfJobsTitle.grid(row = 0, column = 0, pady=(10, 10))

    list_of_jobs = ScrollableFrame(listOfJobsFrame, height = 50, relief = tk.SUNKEN, borderwidth = 3)
    list_of_jobs.grid(row=1, column = 0)

    listOfJobsFrame.grid(row=1, column = 1, pady=(10, 10), padx=(10, 10))
    
    # creating worker status
    listOfWorkersFrame = tk.Frame(master = root)
    listOfWorkersTitle = tk.Label(master = listOfWorkersFrame, text = "Workers Status", font = default_font)
    listOfWorkersTitle.grid(row =0, column = 0, pady = (10, 10))

    list_of_workers = ScrollableFrame(listOfWorkersFrame, height = 50, relief = tk.SUNKEN, borderwidth = 3)
    list_of_workers.grid(row=1, column = 0)

    listOfWorkersFrame.grid(row=1, column = 2, pady=(10, 10), padx=(10, 10))

    # thread untuk monitor workers listing dan status update
    workers_listing_thread = threading.Thread(target = workersListUpdate, args = (list_of_workers.scrollable_frame,), daemon = True)
    workers_label_thread = threading.Thread(target = workersLabelsUpdate, args = (list_of_workers.scrollable_frame,), daemon = True)

    workers_label_thread.start()
    workers_listing_thread.start()

    # add result screen
    resultFrame = tk.Frame(master = root)
    resultTitle = tk.Label(master = resultFrame, text = "Job Result", font = default_font, justify = tk.LEFT, anchor = "w")
    resultTitle.grid(row=0, column = 0, sticky="w")

    resultBox = ScrollableFrame(resultFrame, height = 20, canvas_width = 1000, relief = tk.SUNKEN, borderwidth = 2, bg = "white")
    resultBox.grid(row=1, column = 0, sticky="w")

    resultText = tk.Message(master = resultBox.scrollable_frame, 
        text = """Click on a job to view its detail""",
        font = default_font, justify = tk.LEFT, anchor = "w", background = "white", width =1000)

    
    # add label listeners
    jobs_listing_thread = threading.Thread(target = jobsListUpdate, args = (list_of_jobs.scrollable_frame,resultText), daemon = True)
    jobs_label_thread = threading.Thread(target = jobsLabelsUpdate, args = (list_of_jobs.scrollable_frame,), daemon = True)

    jobs_listing_thread.start()
    jobs_label_thread.start()
    
    
    resultText.pack(fill="both", expand=True)

    resultFrame.grid(row=2, column = 0, columnspan = 3, sticky = "w", padx=(10, 10), pady = (10, 10))

    root.mainloop()

    scheduler.shutdown_server()

