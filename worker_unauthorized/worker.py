import socket, utils
from models import Job
import sys
import time
import threading
import random
from os import getenv

def randomSleep():
    values = [1, 5, 10, 30, 60]
    sleepTime = random.sample(values, 1)[0]
    time.sleep(sleepTime)

def doJob(job):
    print(f"[LOG] Received job with id {j.jobId}")
    res = j.work()
    randomSleep()
    utils.send_message(client, f"{res.encode()}")

def get_data(title):

    ret = input(f"masukkan {title} (atau biarkan kosong untuk nilai default): ")
    if ret != "":
       return ret
    
    raise Exception("Must give valid input")


# command line arguments: CLIENT_PORT
if __name__ == "__main__":
    with socket.socket(socket.AF_INET, socket.SOCK_STREAM) as client:
        #SCHEDULER_HOST = socket.gethostbyname(socket.gethostname()) # Standard loopback interface address (localhost)
        SCHEDULER_HOST = getenv("SCHEDULER_HOST")
        SCHEDULER_PORT = int(getenv("SCHEDULER_PORT"))       # Port to listen on (non-privileged ports are > 1023)
        CLIENT_HOST =  socket.gethostbyname(socket.gethostname())
        CLIENT_PORT = int(get_data("client_port"))


        client.bind((CLIENT_HOST, CLIENT_PORT))
        client.connect((SCHEDULER_HOST, SCHEDULER_PORT))
        
        # can run maximum of ONE job. The multithread is to handle health checking of worker
        while True:
            
            newJob, err = utils.read_message(client)
            if(err):
                utils.send_message(client, "[ERROR] Error invalid HMAC")
                continue

            # print(f"job -{newJob}- recieved")
            j = Job()
            j.decode(newJob)

            doJob(j)
           


