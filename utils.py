import os
from Crypto.Cipher import AES
from Crypto.Hash import HMAC, SHA256
from Crypto.Util.Padding import pad, unpad

SECRET_KEY = open("secret_key.txt", 'rb').read()

HEADER_SIZE = 64    # first send the size of message, then send the message

# utility function untuk send message ke sebuah conn
def send_message(conn, msg):

    # encrypt the payload
    iv = os.urandom(16)
    aes = AES.new(SECRET_KEY, AES.MODE_CBC, iv)
    enc = aes.encrypt(pad(msg.encode('utf-8'), 16))

    # add HMAC
    h = HMAC.new(SECRET_KEY, digestmod=SHA256)
    h.update(enc)
    enc_hash = h.digest()

    message = iv + enc + enc_hash

    message_len = str(len(message))
    message_len = (HEADER_SIZE-len(message_len))*' ' + str(message_len)
    # print(f"sending message len {message_len}")
    # print(f"Encrypted message {message}")
    conn.send(message_len.encode('utf-8'))
    conn.send(message)

# utility function untuk read message dari sebuah conn
def read_message(conn):
    msg_len = ""

    while msg_len == "":
        msg_len = conn.recv(HEADER_SIZE).decode('utf-8')
        #print(f"reading msg_len = {msg_len}")

    #print(f"recieved len: {msg_len},  from {conn}")
    msg_len = int(msg_len)
    msg = conn.recv(msg_len)

    iv = msg[:16]
    enc = msg[16:-32]
    enc_hash = msg[-32:]

    h = HMAC.new(SECRET_KEY, digestmod=SHA256)
    h.update(enc)
    try:
        h.verify(enc_hash)
        aes = AES.new(SECRET_KEY, AES.MODE_CBC, iv)
        message = unpad(aes.decrypt(enc), 16).decode('utf-8')
        return message, False
    except ValueError:
        print("[ERROR] Invalid HMAC")
        return "", True

# helper function to check if connection is dead
def is_alive(conn):
    try:
        send_message(conn, "[HEALTH CHECK]")
    except:
        return False
    
    return True