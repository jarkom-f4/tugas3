import socket
import threading
from threading import Lock
from models import Worker, Job, Answer
import utils
import random
import time


HOST = socket.gethostbyname(socket.gethostname())  # Standard loopback interface address (localhost)
PORT = 18016        # Port to listen on (non-privileged ports are > 1023)
DISCONNECT_MESSAGE = "!DISCONNECT"

mutex = Lock() # mutex agar tidak race condition    
    
jobs = {} # global variable of all current jobs
lastJobId = 0 # increasing job id

def getJobsAsList():
    global mutex
    jobsList = []

    mutex.acquire()
    for job in jobs.values():
        jobsList.append(job)
    
    mutex.release()
    return jobsList

# fungsi multithreading yang handle pesan-pesan yang dikirim dari dari si worker ini
def worker_working(worker, job):

    global mutex
    conn = worker.conn

    # doing the job
    utils.send_message(conn, job.encode())

    msg, err = utils.read_message(conn)
    
    # ini worker unauthorized. Queue job di DEPAN queue
    if err:
        mutex.acquire()
        worker.workerStatus = "Unauthorized"
        jobQueue.insert(0, worker.getJob())
        worker.setJob(None)
        mutex.release()
        return

    if "[ERROR]" in msg:
        msg_tokenized = msg.split(' ')
        error_msg = ' '.join(msg_tokenized)
        jobs[job.jobId].setStatus(f"Failed ({error_msg})")
    else:
        answer = Answer()
        answer.decode(msg)
        jobs[job.jobId].setStatus("Done")
        jobs[job.jobId].setAnswer(answer)

    mutex.acquire()
    worker.workerStatus = "Available"
    worker.setJob(None)
    mutex.release()



workers = {} # dictionary of workers by their workedId(class Worker)
lastWorkerNum = 0 # auto increasing value of worker

def getWorkersAsList():

    global mutex

    workersList = []
    mutex.acquire()
    for worker in workers.values():
        workersList.append(worker)
    mutex.release()

    return workersList


def accept_new_worker(server):
    global workers
    global lastWorkerNum
    global mutex

    while True:
        conn, _ = server.accept()
        w = Worker(lastWorkerNum, conn)

        mutex.acquire()
        workers[lastWorkerNum] = w
        mutex.release()

        lastWorkerNum += 1

        time.sleep(0.05)


jobQueue = [] 
# pending jobs (queueon on master node)
def queue_job(job):

    global mutex

    global jobQueue

    mutex.acquire()
    jobQueue.append(job)
    mutex.release()

# handles the assigment of jobs to workers using FCFS algorithm
def scheduler():
    global jobQueue
    global workers
    global mutex

    # nextWorker = 0
    while True:
        if  len(jobQueue) != 0:
            
            mutex.acquire()
            frontJob = jobQueue[0]
            jobQueue = jobQueue[1:]
            mutex.release()

            # randomly select available worker (load balancer)
            availableWorkers = []

            # loop until available worker exist
            while len(availableWorkers) == 0:
                mutex.acquire()
                for worker in workers.values():

                    # check if worker is still alive (BUG clash when actually busy, so commented)
                    # if (worker.workerStatus == "Available" or worker.workerStatus == "Busy") and not utils.is_alive(worker.conn):
                    #     worker.workerStatus = "Dead"
                    #     if worker.getJob() is not None: # enqueue the job to the back
                    #         jobQueue.append(worker.getJob())
                    #         worker.setJob(None)

                    if worker.workerStatus == "Available":
                        availableWorkers.append(worker)
                            

                mutex.release()
                time.sleep(0.05)
                
            
            sampledWorker = random.sample(availableWorkers, 1)[0]
            sampledWorker.workerStatus = "Busy"
            sampledWorker.setJob(frontJob)
            frontJob.setStatus(f"Running (Worker {sampledWorker.workerNum})")
            

            worker_listener_thread = threading.Thread(target = worker_working, args=(sampledWorker,frontJob, ), daemon=True)
            worker_listener_thread.start()

def shutdown_workers():
    global workers
    for worker in workers.values():
        worker.conn.close()

server = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
def start_server():
    global server
    global mutex
    
    server.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
    server.bind((HOST, PORT))
    server.listen()

    new_workers_thread = threading.Thread(target = accept_new_worker, args=(server,), daemon=True)
    new_workers_thread.start()

    scheduler_thread = threading.Thread(target = scheduler, daemon = True)
    scheduler_thread.start()

def new_job(N, coins, targetValue):
    global lastJobId
    global jobs

    job = Job()
    job.set(N, coins, targetValue, lastJobId)
    lastJobId+=1

    jobs[job.jobId] = job
    queue_job(job)

def shutdown_server():
    global server
    server.shutdown(socket.SHUT_RDWR)
    #shutdown_workers()
    server.close()

if __name__ == "__main__":
    start_server()

    while True:
        cmd = input("masukkan perintah: ")
        if cmd == "EXIT":
            shutdown_server()
            break
        elif cmd == "NEW_JOB":
            N = int(input("masukkan jumlah koin: "))
            coins = [int(x) for x in input("masukkan koin(pisahkan dengan spasi): ").split(' ')]
            targetValue= int(input("masukkan target value: "))
            new_job(N, coins, targetValue)
        elif cmd == "WORKER_STATUS":
            workers = getWorkersAsList()
            for worker in workers:
                print(worker.getStatusStr())
        elif cmd == "JOB_STATUS":
            jobs = getJobsAsList()
            for job in jobs:
                print(job.getStatusStr())
        elif cmd == "GET_JOB":
            jobs = getJobsAsList()
            job = jobs[int(input("masukkan id job yang ingin di lihat hasilnya: "))]
            print(job.getJobInfo())
        else:
            print(f"perintah {cmd} invalid!")
