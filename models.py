import utils

class Worker:
    
    def __init__(self, workerNum, conn):
        self.workerNum = workerNum
        self.conn = conn
        self.workerStatus = "Available"
    
    def setJob(self, job):
        self.currentJob = job
    
    def getJob(self):
        return self.currentJob
        
    def getStatusStr(self):
        return f"Worker {self.workerNum}: {self.workerStatus}"
    
# sebuah coin change job yang di init dengan N dan array sepanjang N berisi jumlah coin
class Job:

    def __init__(self):
        pass

    def set(self, N, coins, targetValue, jobId):
        self.N = N
        self.targetValue = targetValue
        self.coins = coins
        self.jobId = jobId
        self.jobStatus = "Queued"

        self.answer = None
    
    def setStatus(self, newStatus):
        self.jobStatus = newStatus

    # the encoded string to send to worker
    def encode(self):
        coins = [str(coin) for coin in self.coins]
        return str(self.jobId) + " " + str(self.N) +" " + str(self.targetValue)+ " "+' '.join(coins)
    
    # the decoded string that the worker will then decode
    def decode(self, encoded):
        arr = encoded.split(' ')
        self.jobId = int(arr[0])
        self.N = int(arr[1])
        self.targetValue = int(arr[2])
        self.coins = [int(x) for x in arr[3:]]
    
    def getStatusStr(self):
        return f"Job {self.jobId}: {self.jobStatus}"

    def setAnswer(self, answer):
        self.answer = answer

    # doing coin change bottom up
    def work(self):
        
        N = self.N
        targetValue = self.targetValue
        coins = self.coins

        dp = [0 for x in range(0, targetValue+1)]
        backtrack = [0 for x in range(0, targetValue+1)]

        dp[0] = 0;
        for value in range(1, targetValue+1):
            dp[value] = 1000000000
            for coin in coins:
                if coin <= value and (dp[value-coin] + 1 < dp[value]):
                    dp[value] = dp[value-coin] + 1
                    backtrack[value] = coin

        # impossible to construct case
        if dp[targetValue] == 1000000000:
            ans = Answer()
            ans.set(-1, {}, self.jobId)
            return ans

        optCoins = {}
        pos = targetValue
        while pos != 0:
            lastCoin = backtrack[pos]
      
            if lastCoin not in optCoins:
                optCoins[lastCoin] = 0
            
            optCoins[lastCoin] += 1
            pos -= lastCoin

        ans = Answer()
        ans.set(dp[targetValue], optCoins, self.jobId)
        return ans
    
    def getJobInfo(self):
        res = f"Job Number: {self.jobId}\n\n"

        coins_str = [str(coin) for coin in self.coins]

        res += f"Coin values: {' '.join(coins_str)}\n"
        res += f"Target Value: {self.targetValue}\n\n"

        if self.jobStatus == "Queued":
            res += "Job is currently Queued\n"
        elif "Running" in self.jobStatus:
            res += "Job is currently being run by a worker (Check again when done)\n"
        elif self.jobStatus == "Done":

            if self.answer.optNum == -1:
                res+= f"It is impossible to get {self.targetValue} using the available coins"
            else:    
                res += f"Solution can be achieved using {self.answer.optNum} coin"
                if self.answer.optNum > 1: # pluralizer
                    res+="s"
                res+="\n"
            

                for coin, freq in self.answer.optCoins.items():
                    res += f"Coin {coin} is used {freq} time"
                    if freq > 1: # pluralizer
                        res+="s"
                    res+="\n"
        
        return res


        

# sebuah class yang menyimpan jawaban dari sebuah coin change. Atribut: jumlah coin dan array optimal coinnya
class Answer:

    def __init__(self):
        pass

    def set(self, optNum, optCoins, jobId):
        self.optNum = optNum
        self.optCoins = optCoins
        self.jobId = jobId
    
    def encode(self):
        optCoins = ""
        for coin, freq in self.optCoins.items():
            optCoins+=f" {coin} {freq}"

        return str(self.jobId) + " " + str(self.optNum) + optCoins
    
    def decode(self, encoded):
        arr = encoded.split(' ')
        self.jobId = int(arr[0])
        self.optNum = int(arr[1])
        self.optCoins = {}
        arr = arr[2:]

        for i in range(0, len(arr), 2):
            self.optCoins[int(arr[i])] = int(arr[i+1])
        
    
    def __str__(self):
        ret = f"""Optimal number of coins: {self.optNum}\nOptimal coins: {self.optCoins}"""
        return ret
